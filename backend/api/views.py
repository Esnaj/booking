from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework.response import Response
from abc import ABCMeta, abstractmethod
from rest_framework import permissions, status
import logging


class AbstractListView(APIView, metaclass=ABCMeta):

    @property
    @abstractmethod
    def serializer(self):
        raise NotImplementedError

    @property
    @abstractmethod
    def model(self):
        raise NotImplementedError

    def get(self, request, formate=None):
        model_object = self.model.objects.all()
        serialized = self.serializer(model_object, many=True)
        return Response(serialized.data)

    def post(self, request, format=None):
        serialized = self.serializer(data=request.data)
        if serialized.is_valid():
            serialized.save()
            return Response(serialized.data)
        logging.error(msg = serialized.errors)
        return Response(serialized.errors, status=status.HTTP_400_BAD_REQUEST)


class AbstractDetailView(APIView, metaclass=ABCMeta):

    @property
    @abstractmethod
    def serializer(self):
        raise NotImplementedError

    @property
    @abstractmethod
    def model(self):
        raise NotImplementedError

    @property
    def model_object(self):
        try:
            return self.model.objects.get(id=self.kwargs["pk"])
        except self.model.DoesNotExist:
            return None

    def get(self, request, pk, format=None):
        if self.model_object is None:
            return Response(status=status.HTTP_404_NOT_FOUND)
        serialized = self.serializer(self.model_object, many=False)
        return Response(serialized.data)

    def patch(self, request, pk, format=None):
        if self.model_object is None:
            return Response(status=status.HTTP_404_NOT_FOUND)
        serialized = self.serializer(
            self.model_object, data=request.data, partial=True)

        if serialized.is_valid():
            serialized.save()
            return Response(serialized.data)
        else:
            return Response(serialized.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        if self.model_object is None:
            return Response(status=status.HTTP_404_NOT_FOUND)

        self.model_object.delete()
        return Response(status.HTTP_200_OK)

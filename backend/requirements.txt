# apturl==0.5.2
asgiref==3.3.1
astroid==2.5.1
attrs==19.3.0
autopep8==1.5.6
beautifulsoup4==4.8.2
blinker==1.4
# Brlapi==latest
certifi==2019.11.28
chardet==3.0.4
Click==7.0
colorama==0.4.3
coverage==5.2.1
djangorestframework-simplejwt==4.4.0
PyJWT==1.7.1

# command-not-found==0.3
# cryptography==2.8
# cupshelpers==1.0
# cycler==0.10.0
# dbus-python==1.2.16
# decorator==4.4.2
# defer==1.0.6
# distro==1.4.0
# distro-info===0.23ubuntu1
Django==3.1.7
django-filter==2.4.0
djangorestframework==3.12.4
# entrypoints==0.3
# Flask==1.1.2
# Flask-Cors==3.0.10
html5lib==1.0.1
httplib2==0.14.0
# idna==2.8
# image-compression==0.4
# importlib-metadata==1.5.0
# isort==5.7.0
# itsdangerous==1.1.0
# Jinja2==2.11.3
# jsonschema==3.2.0
# keyring==18.0.1
# kiwisolver==1.0.1
# language-selector==0.1
# launchpadlib==1.10.13
# lazr.restfulclient==0.14.2
# lazr.uri==1.0.3
# lazy-object-proxy==1.5.2
# louis==3.12.0
# lxml==4.5.0
# macaroonbakery==1.3.1
# Markdown==3.1.1
# MarkupSafe==1.1.1
# matplotlib==3.1.2
# mccabe==0.6.1
# more-itertools==4.2.0
# netifaces==0.10.4
# numpy==1.17.4
# oauthlib==3.1.0
# olefile==0.46
# packaging==20.3
# pexpect==4.6.0
# Pillow==7.0.0
# protobuf==3.6.1
# PyAudio==0.2.11
# pycairo==1.16.2
# pycodestyle==2.7.0
# pycups==1.9.73
# Pygments==2.3.1
# PyGObject==3.36.0
# PyJWT==1.7.1
# pylint==2.7.2
# pymacaroons==0.13.0
# PyNaCl==1.3.0
# pyparsing==2.4.6
# PyQt5==5.14.1
# PyQtWebEngine==5.14.0
# pyRFC3339==1.1
# pyrsistent==0.15.5
# python-apt==2.0.0+ubuntu0.20.4.4
# python-dateutil==2.7.3
# python-debian===0.1.36ubuntu1
# pytz==2019.3
# pyxattr==0.6.1
# pyxdg==0.26
# PyYAML==5.3.1
# reportlab==3.5.34
# requests==2.22.0
# requests-unixsocket==0.2.0
# SecretStorage==2.3.1
# Send2Trash==1.5.0
# simplejson==3.16.0
# sip==4.19.21
# six==1.14.0
# soupsieve==1.9.5
# sqlparse==0.4.1
# systemd-python==234
# toml==0.10.2
# ubuntu-advantage-tools==27.0
# ubuntu-drivers-common==0.0.0
# ufw==0.36
# unattended-upgrades==0.1
# urllib3==1.25.8
# wadllib==1.3.3
# webencodings==0.5.1
# Werkzeug==1.0.1
# wrapt==1.12.1
# xkit==0.0.0
# zipp==1.0.0
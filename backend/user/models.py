from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin


# Create your models here.
class CustomAccountManager(BaseUserManager):

    def create_superuser(self, user_name, password, **kwargs):
        kwargs.setdefault('is_staff', True)
        kwargs.setdefault('is_superuser', True)
        kwargs.setdefault('is_active', True)

        return self.create_user(user_name=user_name, password=password, **kwargs)

    def create_user(self, user_name, password, **kwargs):
        user = self.model(user_name=user_name, **kwargs)
        user.set_password(password)
        user.save()
        return user
        
    
class User(AbstractBaseUser, PermissionsMixin):

    user_name = models.CharField(max_length=100, unique=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)    

    objects = CustomAccountManager()

    USERNAME_FIELD = 'user_name'
    # REQUIRED_FIELDS = ['user_name']

    def __str__(self):
        return self.user_name
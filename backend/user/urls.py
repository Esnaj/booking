from django.conf.urls import include
from django.contrib import admin
from django.urls.conf import re_path
from django.urls import path
from .views import CustomUserCreate, BlacklistTokenUpdateView, HiView
from rest_framework.schemas import get_schema_view
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

urlpatterns = [
    path('create/', CustomUserCreate.as_view(), name="create_user"),
    path('hi/', HiView.as_view(), name="create_user"),
    path('logout/blacklist/', BlacklistTokenUpdateView.as_view(),
         name='blacklist'),
    path('auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

]

# from django.db.models.fields import DateTimeField
# from booking.cron import create_booking_week
# from datetime import date, datetime
# from re import A
# from booking.serializers import BookingSerializer, BookingTestSerializer, SlotSerializer
# from django.test import TestCase
# from rest_framework.test import APIRequestFactory
# from .models import Booking, DateTimeBooking, Slot

# import json
# from rest_framework import status
# from django.test import TestCase, Client
# from django.urls import reverse


# # initialize the APIClient app
# client = Client()

# # Create your tests here.
# factory = APIRequestFactory()


# class GetAllPuppiesTest(TestCase):
#     """ Test module for GET all puppies API """

#     def setUp(self):
#         Booking.objects.create(name="booking1")
#         Booking.objects.create(name="booking2")

#     def test_get_all_puppies(self):
#         # get API response
#         response = client.get(reverse('get_bookings'))
#         # get data from db
#         bookings = Booking.objects.all()
#         serializer = BookingSerializer(bookings, many=True)
#         # self.assertEqual(response.data, serializer.data)
#         self.assertEqual(response.status_code, status.HTTP_200_OK)


# class GetDetailBooking(TestCase):
#     """ Test module for GET all puppies API """

#     def setUp(self):
#         Booking.objects.create(name="booking1")

#     def test_get_slots_from_booking(self):
#         booking = Booking.objects.create(name="newBooking")
#         Slot.objects.create(booking=booking)
#         response = client.get(
#             reverse('get_booking', kwargs={'pk': booking.id}))
#         self.assertEqual(len(response.data['slots']), 1)

#     def test_get_available_slots(self):
#         booking = Booking.objects.create(name="newBooking")
#         response = client.get(
#             reverse('get_booking', kwargs={'pk': booking.id}))
#         self.assertEqual(response.data['available_slots'], 1)

#     def test_get_available_slots_after_adding_slot(self):
#         booking = Booking.objects.create(name="newBooking")
#         Slot.objects.create(booking=booking)
#         response = client.get(
#             reverse('get_booking', kwargs={'pk': booking.id}))
#         self.assertEqual(response.data['available_slots'], 0)

#     def test_get_detail_booking(self):
#         response = client.get(reverse('get_booking', kwargs={'pk': 1}))
#         booking = Booking.objects.first()
#         serializer = BookingSerializer(booking, many=False)
#         # self.assertEqual(response.data, serializer.data)
#         self.assertEqual(response.status_code, status.HTTP_200_OK)

#     def test_delete_detail_booking(self):
#         response = client.delete(reverse('get_booking', kwargs={'pk': 1}))

#         count = Booking.objects.all().count()
#         self.assertEqual(count, 0)
#         self.assertEqual(response.status_code, status.HTTP_200_OK)

#     def test_patch_detail_booking(self):
#         valid_payload = {
#             'name': 'Muffin',
#         }
#         response = client.patch(
#             reverse('get_booking', kwargs={'pk': 1}),
#             data=json.dumps(valid_payload),
#             content_type='application/json'
#         )
#         booking = Booking.objects.first()
#         self.assertEqual(booking.name, 'Muffin')
#         self.assertEqual(response.status_code, status.HTTP_200_OK)

#     def test_bad_patch_detail_booking(self):
#         invalid_payload = {
#             'nSlots': 'Muffin',
#         }
#         response = client.patch(
#             reverse('get_booking', kwargs={'pk': 1}),
#             data=json.dumps(invalid_payload),
#             content_type='application/json'
#         )
#         booking = Booking.objects.first()
#         self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


# class SlotTests(TestCase):
#     def setUp(self):
#         self.booking = Booking.objects.create(name="booking1")
#         self.other_booking = Booking.objects.create(name="otherbooking")

#     def test_create_slot(self):
#         Slot.objects.create(booking=self.booking, name="slot1")
#         self.assertEqual(Slot.objects.first().name, "slot1")

#     def test_slot_get(self):

#         slot1 = Slot.objects.create(booking=self.booking, name="slot1")
#         Slot.objects.create(booking=self.other_booking, name="otherslot")
#         response = client.get(
#             reverse('booking_slot', kwargs={'booking_id': 1}),
#             content_type='application/json',
#         )

#         self.assertEqual(len(response.data), 1)

#     def test_slot_post(self):
#         valid_data = {
#             # "booking_id": 1,
#             "name": "posted_slot"
#         }
#         nSlots = Slot.objects.count()
#         response = client.post(
#             reverse('booking_slot', kwargs={'booking_id': 1}),
#             data=json.dumps(valid_data),
#             content_type='application/json'
#         )
#         self.assertEqual(nSlots, Slot.objects.count() - 1)
#         self.assertEqual(response.status_code, status.HTTP_200_OK)

#     def test_patch_slot(self):
#         valid_data = {
#             "name": "updated_slot",
#             "booking_id": 1,
#         }
#         slot = Slot.objects.create(booking=self.booking, name="slot1")
#         response = client.patch(
#             reverse('detail_slot', kwargs={'pk': slot.id}),
#             data=json.dumps(valid_data),
#             content_type='application/json'
#         )

#         slot = Slot.objects.get(pk=slot.id)
#         self.assertEqual(slot.name, valid_data['name'])
#         self.assertEqual(response.status_code, status.HTTP_200_OK)

#     def test_slot_post_1(self):
#         booking_1 = Booking.objects.create(name="booking_1", nSlots=0)

#         valid_data = {
#             "booking_id": booking_1.pk,
#             "name": "posted_slot"
#         }

#         nSlots = Slot.objects.count()
#         response = client.post(
#             reverse('booking_slot', kwargs={'booking_id': booking_1.pk}),
#             data=json.dumps(valid_data),
#             content_type='application/json'
#         )
#         self.assertEqual(nSlots, Slot.objects.count())
#         self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

#     def test_slot_post_1(self):
#         booking_1 = Booking.objects.create(name="booking_1", nSlots=0)

#         valid_data = {
#             "booking_id": booking_1.pk,
#             "name": "posted_slot"
#         }

#         nSlots = Slot.objects.count()
#         response = client.post(
#             reverse('booking_slot', kwargs={'booking_id': booking_1.pk}),
#             data=json.dumps(valid_data),
#             content_type='application/json'
#         )
#         self.assertTrue(response.has_header)
#         self.assertEqual(response['message'], "Booking is fully reserved!")
#         self.assertEqual(nSlots, Slot.objects.count())
#         self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


# class serializerTests(TestCase):
#     def setUp(self):
#         self.booking = Booking.objects.create(name="booking1")

#     def test_serialize_booking(self):
#         serialized = BookingTestSerializer(self.booking)
#         data = serialized.data
#         self.assertTrue('test' in data.keys())

#     def test_deserialzie_booking(self):
#         valid_data = {
#             'name': 'henky',
#             'nSlots': 4
#         }
#         deserialized = BookingTestSerializer(data=valid_data)
#         deserialized.is_valid()
#         saved_booking = deserialized.save()
#         self.assertTrue(saved_booking.name, valid_data['name'])


# class dateTimeBookingTests(TestCase):
#     def setUp(self):
#         today = datetime.today()
#         self.a_time_instance = datetime.now().time()
#         self.datetimeBooking = DateTimeBooking.objects.create(
#             date=today,
#             time=self.a_time_instance)

#     def test_datetimebooking(self):

#         self.datetimeBooking = DateTimeBooking.objects.create(
#             date=datetime.today(), time=self.a_time_instance)
#         response = client.get(
#             reverse('get_datetimebookings')
#         )
#         self.assertEquals(len(response.data), 2)

#     def test_datetimebooking_post(self):
#         valid_data = {
#             "name": 'timebook',
#             "date": datetime.today().date(),
#             "time": self.a_time_instance
#         }

#         response = client.post(
#             reverse('get_datetimebookings'),
#             data=valid_data
#         )
#         self.assertEquals(DateTimeBooking.objects.all().count(), 2)


#     def test_check_len_of_date_filtering_all_datetimebookings(self):
#         self.datetimeBooking = DateTimeBooking.objects.create(
#             date=date(1999,3,10), time=self.a_time_instance)
#         self.datetimeBooking = DateTimeBooking.objects.create(
#             date=date(1999,3,10), time=self.a_time_instance)
#         self.datetimeBooking = DateTimeBooking.objects.create(
#             date=date(1999,3,10), time=self.a_time_instance)
#         self.assertEqual(len(DateTimeBooking.objects.filter(date = date(1999,3,10))), 3)


#     def test_datetimebooking_detail(self):
#         response = client.get(
#             reverse('get_datetimebooking', kwargs={
#                     'pk': self.datetimeBooking.id})
#         )
#         self.assertEqual(response.data['name'], self.datetimeBooking.name)

#     def test_datetimebooking_on_date(self):
#         new_booking = DateTimeBooking.objects.create(
#             name='a_new_booking', date="1999-03-20", time = self.a_time_instance)
#         response = client.get(
#             reverse('get_datetimebooking_on_date', kwargs={
#                 'year': '1999',
#                 'month': '03',
#                 'day': '20',
#             })
#         )
#         self.assertEqual(response.data[0]['name'], new_booking.name)


# class CronTests(TestCase):
#     def test_read_json(self):
#         n_bookings = Booking.objects.all().count()
#         create_booking_week(10)
#         n_bookings_after = Booking.objects.all().count()
#         self.assertGreater(n_bookings_after, n_bookings)

#     def test_auto_add_bookings_shouldnt_add_duplicate(self):
#         create_booking_week(10)
#         n_bookings = Booking.objects.all().count()
#         create_booking_week(10)
#         n_bookings_after = Booking.objects.all().count()
#         self.assertEqual(n_bookings_after, n_bookings)
    
#     def test_auto_add_bookings_should_add_more_than_100(self):
#         create_booking_week(10)
#         n_bookings = Booking.objects.all().count()
#         self.assertGreater(n_bookings, 10)


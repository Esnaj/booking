from booking.models import Booking
from booking.serializers import BookingSerializer
import datetime
from rest_framework.response import Response
from event.models import Event
from event.serializers import EventSerializer
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework import serializers, status
from rest_framework.permissions import AllowAny, IsAuthenticated


class EventAddBookingView(APIView):
    serializer = BookingSerializer
    model = Booking

    def post(self, request, *args, **kwargs):
        permission_classes = [AllowAny]
        data = request.data.copy()
        data['event'] = kwargs['id']

        serialized = self.serializer(data=data)

        if serialized.is_valid():
            serialized.save()
            return Response(status=status.HTTP_200_OK)
        return Response(serialized.errors, status=status.HTTP_400_BAD_REQUEST)


# Create your views here.


# class Booking(APIView):
#     serializer = EventSerializer
#     model = Event

#     def get(self, request, year, month, day, format=None):
#         year, month, day = int(year), int(month), int(day)
#         date = datetime.date(year=year, month=month, day=day)
#         events = Event.objects.filter(date=date)
#         serialized = self.serializer(events, many=True)
#         return Response(serialized.data, status.HTTP_200_OK)


# from rest_framework import serializers, status
# from rest_framework.response import Response
# from api.views import AbstractDetailView, AbstractListView
# from django.shortcuts import render
# from .serializers import BookingSerializer, DateTimeBookingSerializer, SlotSerializer
# from .models import Booking, Slot, DateTimeBooking
# import logging

# # Create your views here.


# class BookingListView(AbstractListView):
#     serializer = BookingSerializer
#     model = Booking


# class DateTimeBookingListView(AbstractListView):
#     serializer = DateTimeBookingSerializer
#     model = DateTimeBooking


# class DateBookingOnDateListView(AbstractListView):
#     serializer = DateTimeBookingSerializer
#     model = DateTimeBooking

#     def get(self, request, year, month, day, format=None):
#         date = str(year) + "-" + str(month) + "-" + str(day)
#         model_object = self.model.objects.filter(date=date)
#         serialized = self.serializer(model_object, many=True)
#         return Response(serialized.data)


# class DateTimeBookingDetailView(AbstractDetailView):
#     serializer = DateTimeBookingSerializer
#     model = DateTimeBooking


# class BookingDetailView(AbstractDetailView):
#     serializer = BookingSerializer
#     model = Booking


# class BookingSlotListView(AbstractListView):
#     serializer = SlotSerializer
#     model = Slot
#     bookingModel = Booking

#     def get(self, request, booking_id, format=None):

#         model_object = self.model.objects.filter(booking_id=booking_id)
#         serialized = self.serializer(model_object, many=True)
#         return Response(serialized.data)

#     def post(self, request, booking_id, format=None):
#         data = request.data
#         data["booking"] = booking_id
#         serialized = self.serializer(data=data)
#         if serialized.is_valid():
#             filteredBooking = self.model.objects.filter(booking_id=booking_id)
#             bookedSlot = filteredBooking.count()

#             bookingObject = self.bookingModel.objects.get(pk=booking_id)

#             if bookedSlot >= bookingObject.nSlots:
#                 return Response(serialized.data, status=status.HTTP_400_BAD_REQUEST, headers={"message": "Booking is fully reserved!"})

#             serialized.save()
#             return Response(serialized.data)

#         return Response(serialized.errors, status=status.HTTP_400_BAD_REQUEST)


# class SlotDetailView(AbstractDetailView):
#     serializer = SlotSerializer
#     model = Slot

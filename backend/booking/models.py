import event.models as event
from django.db import models
from django.db.models.fields import BooleanField, CharField, DateField, DateTimeField, EmailField, IntegerField, TimeField
from django.db.models.fields.related import ForeignKey
from datetime import datetime
import django.utils.timezone as tz

# Create your models here.

class Booking(models.Model):
    name = CharField(max_length=100)
    email = EmailField(null=True)
    event = ForeignKey(event.Event, on_delete=models.CASCADE)

# class Slot(models.Model):
#     booking = ForeignKey(Booking, on_delete=models.CASCADE)
#     name = CharField(max_length=100, default="NO_NAME")
#     # isTaken = BooleanField(default=False)


# class DateTimeBooking(Booking):
#     date = DateField(tz.now())
#     time = TimeField()

from rest_framework import serializers
from event.serializers import EventSerializer
from booking.models import Booking
import datetime
from event.models import Event
from django.test import TestCase
from booking.serializers import BookingSerializer


class BookingSerializerTest(TestCase):

    def setUp(self):
        date = datetime.date(2012, 1, 1)
        time = datetime.time(hour=10, minute=0)
        self.event = Event.objects.create(nBookings=5, time=time, date=date)
        self.event2 = Event.objects.create(nBookings=0, time=time, date=date)
        self.booking = Booking.objects.create(name='henky', event=self.event)

    def test_serializer_py_to_json(self):
        serialized = BookingSerializer(self.booking)
        self.assertEquals(serialized.data['name'], self.booking.name)
    
    def test_serializer_json_to_py_valid(self):
        valid_data = {
            'name':'andre',
            'event':self.event.id
        }
        serialized = BookingSerializer(data=valid_data)
        self.assertTrue(serialized.is_valid())

    
    def test_serializer_json_to_py_invalid(self):
        valid_data = {
            'name':'andre',
            'FALSE DATA': 30
        }
        serialized = BookingSerializer(data=valid_data)
        self.assertFalse(serialized.is_valid())


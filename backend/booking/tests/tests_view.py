import json
from booking.models import Booking
import datetime
from event.models import Event
from django.test import TestCase
from django.urls import reverse
from django.test import TestCase, Client
from rest_framework import serializers, status

# initialize the APIClient app
client = Client()


class EventTest(TestCase):

    def setUp(self):
        date = datetime.date(2012, 1, 1)
        self.date = date
        time = datetime.time(hour=10, minute=0)
        self.event = Event.objects.create(nBookings=5, time=time, date=date)
        self.booking = Booking.objects.create(name='henky', event=self.event)
        self.event2 = Event.objects.create(nBookings=0, time=time, date=date)

    def post_request_maker(self, data):
        return client.post(
            reverse('event_add_booking', kwargs={'id': self.event.id}),
            data=json.dumps(data),
            content_type='application/json',
        )

    def test_add_booking_valid(self):
        data = {
            'name': 'Andre'
        }
        response = self.post_request_maker(data)
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_add_booking_valid_in_db(self):
        data = {
            'name': 'Andre'
        }
        bookings_before = self.event.get_n_taken_bookings()
        self.post_request_maker(data)
        bookings_after = self.event.get_n_taken_bookings()
        self.assertEquals(bookings_before + 1, bookings_after)

    def test_add_booking_bad_request(self):
        data = {
            'BAD_DATA': 'BAD DATA'
        }
        response = self.post_request_maker(data)
        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)

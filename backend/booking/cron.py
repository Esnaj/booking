from .models import Booking, DateTimeBooking
import logging
from datetime import datetime, timedelta, time


def addBooking():
    Booking.objects.create(name="cron added!")
    print('helloooo')


def my_scheduled_job():
    create_booking_week(100)


def create_booking_week(n_days_in_future: int) -> None:
    date = datetime.today().date()

    for i in range(n_days_in_future):
        currentdate = date + timedelta(days=i)
        if DateTimeBooking.objects.filter(date=currentdate).count() == 0:
            weekday_n = currentdate.weekday()

            for j in weekly_schedule[weekday_n]:
                [hours, minutes, seconds] = [int(x) for x in j.split(':')]
                new_time = time(hours, minutes, seconds)
                x = DateTimeBooking.objects.create(
                    name="new datebooking", date=currentdate, time=new_time)


hourly_schedule = [
    '9:00:00',
    '10:00:00',
    '11:00:00',
    '12:00:00',
    '13:00:00',
    '14:00:00',
    '15:00:00',
    '16:00:00',
    '17:00:00',
]

weekly_schedule = [
    hourly_schedule,
    hourly_schedule,
    hourly_schedule,
    hourly_schedule,
    hourly_schedule,
    [],
    [],
]

# weekly_schedule = {
#     "monday": hourly_schedule,
#     "tuesday": hourly_schedule,
#     "wednesday": hourly_schedule,
#     "thursday": hourly_schedule,
#     "friday": hourly_schedule,
#     "saturday": [],
#     "sunday": [],
# }

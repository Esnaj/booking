from rest_framework import serializers
from .models import Booking


class BookingSerializer(serializers.ModelSerializer):

    class Meta:
        model = Booking
        fields = '__all__'

# class BookingSerializer(serializers.ModelSerializer):
#     slots = serializers.SerializerMethodField(read_only=True)
#     available_slots = serializers.SerializerMethodField(read_only=True)

#     class Meta:
#         model = Booking
#         fields = ['id', 'name', 'nSlots', 'slots', 'available_slots']
#         read_only_fields = ['slots', 'available_slots']

#     def get_slots(self, obj):
#         slots = Slot.objects.filter(booking_id=obj.id)
#         serialized_slots = SlotSerializer(slots, many=True)
#         return serialized_slots.data

#     def get_available_slots(self, obj):
#         return obj.nSlots - Slot.objects.filter(booking_id=obj.id).count()


# class DateTimeBookingSerializer(BookingSerializer):
#     class Meta:
#         model = DateTimeBooking
#         fields = ['id', 'date', 'name', 'nSlots', 'slots', 'available_slots', 'time']
#         read_only_fields = ['slots', 'available_slots']


# class SlotSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Slot
#         fields = '__all__'


# class BookingTestSerializer(serializers.ModelSerializer):
#     test = serializers.CharField(default="hello", read_only=True)

#     class Meta:
#         model = Booking
#         fields = ['id', 'name', 'nSlots', 'test']
#         read_only_fields = ['test']

# from django.test import TestCase
# from .models import Booking, NoAvaiableReservationsSlots, Reservation, Worker, WorkerIsNotAvailable, Slot
# from django.db.utils import IntegrityError

# # Create your tests here.


# class BookingTest(TestCase):

#     def setUp(self):
#         self.somebooking = Booking.objects.create(time='09:00')
#         self.worker = Worker.objects.create(name="Henky")
#         self.somebooking.addWorker(self.worker)

#     def test_worker_is_available_false(self):
#         slot = self.somebooking.getSlots().first()
#         Reservation.objects.create(slot=slot)
#         self.assertFalse(self.worker.isAvailable(self.somebooking))

#     def test_worker_is_available_true(self):
#         self.assertTrue(self.worker.isAvailable(self.somebooking))

#     def test_no_two_slots_for_same_booking_and_worker(self):
#         self.assertRaises(IntegrityError, lambda: self.somebooking.addWorker(self.worker))

#     def test_cant_have_multiple_reservations_same_worker_and_booking(self):
#         slot = self.somebooking.getSlots().first()
#         Reservation.objects.create(slot=slot)
#         self.assertRaises(IntegrityError, lambda: Reservation.objects.create(slot=slot))
    
#     def test_res_after_slot_delete(self):
#         slot = self.somebooking.getSlots().first()
#         Reservation.objects.create(slot=slot)
#         slot.delete()


from typing import Callable
from django.db import models
from django.db.models.deletion import CASCADE
from django.db.models.fields import CharField, DateField, IntegerField, TimeField
from django.db.models.fields.related import ManyToManyField
import django.utils.timezone as tz
from datetime import date, datetime


class Booking(models.Model):
    date = DateField(default = tz.now)
    time = TimeField()

    def addWorker(self, worker):
        Slot.objects.create(booking=self, worker=worker)

    def getSlots(self):
        return Slot.objects.filter(booking=self)

    def getNSlots(self) -> int:
        return self.getSlots().count()


    
class Worker(models.Model):
    name = CharField(max_length=50)
    
    def isAvailable(self, booking: Booking) -> bool:
        # print(Slot.objects.filter(booking=booking, worker=self))
        slot = Slot.objects.get(booking=booking, worker=self)

        if Reservation.objects.filter(slot = slot):
            return False
        return True 
 

    
        
class Slot(models.Model):
    booking = models.ForeignKey(Booking, on_delete=models.CASCADE)
    worker = models.ForeignKey(Worker, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('booking', 'worker', )
    
    def getReservation(self):
        return Reservation.objects.get(slot=self)

    def save(self, *args, **kwargs):
        super(Slot, self).save(*args, **kwargs)
    

        

class Reservation(models.Model):
    name = CharField(max_length=100, default="henky")
    slot = models.OneToOneField(Slot, on_delete=CASCADE)


class NoAvaiableReservationsSlots(Exception):
    pass


class WorkerIsNotAvailable(Exception):
    pass

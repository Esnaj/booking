from django.apps import AppConfig


class WorkerbookingConfig(AppConfig):
    name = 'workerbooking'

from event.models import Event
import logging
from datetime import datetime, timedelta, time


def addBooking():
    Event.objects.create(name="cron added!")
    print('helloooo')


def my_scheduled_job():
    logging.log(logging.CRITICAL, "Cronjob Executing...")
    create_booking_week(100)
    logging.log(logging.CRITICAL, "Cronjob Executed!")
    print('hi')


def create_booking_week(n_days_in_future: int, nBookings: int = 0) -> None:
    date = datetime.today().date()

    for i in range(n_days_in_future):
        currentdate = date + timedelta(days=i)
        if Event.objects.filter(date=currentdate).count() == 0:
            weekday_n = currentdate.weekday()

            for j in weekly_schedule[weekday_n]:
                [hours, minutes, seconds] = [int(x) for x in j.split(':')]
                new_time = time(hours, minutes, seconds)
                x = Event.objects.create(
                    nBookings=nBookings, date=currentdate, time=new_time)


hourly_schedule = [
    '9:00:00',
    '10:00:00',
    '11:00:00',
    '12:00:00',
    '13:00:00',
    '14:00:00',
    '15:00:00',
    '16:00:00',
    '17:00:00',
]

weekly_schedule = [
    hourly_schedule,
    hourly_schedule,
    hourly_schedule,
    hourly_schedule,
    hourly_schedule,
    [],
    [],
]
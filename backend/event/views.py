import datetime
from rest_framework.response import Response
from event.models import Event
from event.serializers import EventDetailSerializer, EventSerializer
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework import status

# Create your views here.


class EventOnDateListView(APIView):
    serializer = EventSerializer
    model = Event

    def get(self, request, year, month, day, format=None):
        year, month, day = int(year), int(month), int(day)
        date = datetime.date(year=year, month=month, day=day)
        events = Event.objects.filter(date=date)
        serialized = self.serializer(events, many=True)
        return Response(serialized.data, status.HTTP_200_OK)


class EventDetailView(APIView):
    serializer = EventDetailSerializer
    model = Event

    @property
    def event(self):
        try:
            return self.model.objects.get(id=self.kwargs["id"])
        except self.model.DoesNotExist:
            return None

    def get(self, request, id, format=None):
        if not self.event:
            return Response(status.HTTP_404_NOT_FOUND)
        serialized = self.serializer(self.event)
        return Response(serialized.data, status.HTTP_200_OK)

    def patch(self, request, id, format=None):
        if not self.event:
            return Response(status.HTTP_404_NOT_FOUND)

        serialized = self.serializer(
            self.event, data=request.data, partial=True)

        if serialized.is_valid():
            serialized.save()
            return Response(serialized.data, status.HTTP_204_NO_CONTENT)
        else:
            return Response(serialized.errors, status=status.HTTP_400_BAD_REQUEST)

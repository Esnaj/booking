from booking.serializers import BookingSerializer
from rest_framework import serializers
from .models import Event


class EventSerializer(serializers.ModelSerializer):
    """
    Should only be used for GET requests
    """
    n_taken_bookings = serializers.SerializerMethodField(read_only=True)
    is_available = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Event
        fields = ['id', 'date', 'time', 'nBookings', 'n_taken_bookings', 'is_available']
        read_only_fields = ['n_taken_bookings', 'is_available']

    def get_n_taken_bookings(self, obj):
        return obj.get_n_taken_bookings()

    def get_is_available(self, obj):
        return obj.is_available()
        

class EventDetailSerializer(serializers.ModelSerializer):
    """
    Should only be used for GET requests
    """
    n_taken_bookings = serializers.SerializerMethodField(read_only=True)
    is_available = serializers.SerializerMethodField(read_only=True)
    bookings = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Event
        fields = ['id', 'date', 'time', 'nBookings', 'n_taken_bookings', 'is_available', 'bookings']
        read_only_fields = ['n_taken_bookings', 'is_available', 'bookings']

    def get_bookings(self, obj: Event):
        bookings = obj.get_bookings()
        x = BookingSerializer(bookings, many=True)
        return x.data
        


    def get_n_taken_bookings(self, obj):
        return obj.get_n_taken_bookings()

    def get_is_available(self, obj):
        return obj.is_available()
        

import json
from booking.models import Booking
import datetime
from event.models import Event
from django.test import TestCase
from django.urls import reverse
from django.test import TestCase, Client


# initialize the APIClient app
client = Client()


class EventTest(TestCase):

    def setUp(self):
        date = datetime.date(2012, 1, 1)
        self.date = date
        time = datetime.time(hour=10, minute=0)
        self.event = Event.objects.create(nBookings=5, time=time, date=date)
        self.booking = Booking.objects.create(name='henky', event=self.event)
        self.event2 = Event.objects.create(nBookings=0, time=time, date=date)

    def test_get_on_date_with_match(self):
        response = client.get(reverse('get_events_on_date', kwargs={
            'year': '2012',
            'month': '01',
            'day': '01',
        }))
        self.assertEqual(len(response.data), 2)

    def test_get_on_date_no_match(self):
        response = client.get(reverse('get_events_on_date', kwargs={
            'year': '2013',
            'month': '01',
            'day': '01',
        }))
        self.assertEqual(len(response.data), 0)

    def test_detail_get(self):
        response = client.get(
            reverse('event_detail', kwargs={'id': self.event.id}))
        self.assertEqual(response.data['id'], self.event.id)

    def test_detail_get_with_booking_data(self):
        response = client.get(
            reverse('event_detail', kwargs={'id': self.event.id}))
        self.assertEqual(response.data['bookings']
                         [0]['name'], self.booking.name)

    def test_detail_get_with_booking_data_with_no_bookings(self):
        response = client.get(
            reverse('event_detail', kwargs={'id': self.event2.id}))
        self.assertEqual(len(response.data['bookings']), 0)

    def test_detail_patch_returns_correct_amount(self):
        data = {
            'nBookings': 10
        }
        response = client.patch(
            reverse('event_detail', kwargs={'id': self.event.id}),
            data=json.dumps(data),
            content_type='application/json',
        )
        self.assertEqual(response.data['nBookings'], 10)

    def test_detail_patch_returns_correct_amount_for_model(self):
        data = {
            'nBookings': 10
        }
        response = client.patch(
            reverse('event_detail', kwargs={'id': self.event.id}),
            data=json.dumps(data),
            content_type='application/json',
        )
        x = Event.objects.get(id=self.event.id).nBookings
        self.assertEqual(x, data['nBookings'])

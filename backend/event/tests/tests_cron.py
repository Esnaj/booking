from event.models import Event
from event.cron import create_booking_week
from re import A
from django.test import TestCase
from rest_framework.test import APIRequestFactory

from django.test import TestCase, Client


# initialize the APIClient app
client = Client()

# Create your tests here.
factory = APIRequestFactory()


class CronTests(TestCase):
    def test_read_json(self):
        n_events = Event.objects.all().count()
        create_booking_week(10)
        n_events_after = Event.objects.all().count()
        self.assertGreater(n_events_after, n_events)

    def test_auto_add_bookings_shouldnt_add_duplicate(self):
        create_booking_week(10)
        n_events = Event.objects.all().count()
        create_booking_week(10)
        n_events_after = Event.objects.all().count()
        self.assertEqual(n_events, n_events_after)

    def test_auto_add_bookings_should_add_more_than_100(self):
        create_booking_week(10)
        n_events = Event.objects.all().count()
        self.assertGreater(n_events, 10)

from booking.models import Booking
import datetime
from event.models import Event
from django.test import TestCase


class EventTest(TestCase):

    def setUp(self):
        date = datetime.date(2012, 1, 1)
        time = datetime.time(hour=10, minute=0)
        self.event = Event.objects.create(nBookings=5, time=time, date=date)
        self.booking = Booking.objects.create(name='henky', event=self.event)
        self.event2 = Event.objects.create(nBookings=0, time=time, date=date)

    def test_create_booking(self):
        self.event
        pass

    def test_get_n_bookings(self):
        x = self.event.get_bookings().first()
        self.assertEquals(x, self.booking)

    def test_get_available_bookings(self):
        x = self.event.get_n_available_bookings()
        self.assertEquals(x, 4)

    def test_get_n_taken_bookings(self):
        x = self.event.get_n_taken_bookings()
        self.assertEquals(x, 1)

    def test_get_is_available_true(self):
        x = self.event.is_available()
        self.assertTrue(x)

    def test_get_is_available_false(self):
        x = self.event2.is_available()
        self.assertFalse(x)

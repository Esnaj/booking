from event.serializers import EventSerializer
from booking.models import Booking
import datetime
from event.models import Event
from django.test import TestCase


class SerializerTest(TestCase):

    def setUp(self):
        date = datetime.date(2012, 1, 1)
        time = datetime.time(hour=10, minute=0)
        self.event = Event.objects.create(nBookings=5, time=time, date=date)
        self.event2 = Event.objects.create(nBookings=0, time=time, date=date)
        self.booking = Booking.objects.create(name='henky', event=self.event)

    def test_serializer_is_available_true(self):
        x = EventSerializer(self.event)
        self.assertTrue(x.data['is_available'])

    def test_serializer_is_available_false(self):
        x = EventSerializer(self.event2)
        self.assertFalse(x.data['is_available'])

import booking.models as booking
from django.db import models
from django.db.models.fields import BooleanField, CharField, DateField, DateTimeField, EmailField, IntegerField, TimeField
from django.db.models.fields.related import ForeignKey
from datetime import datetime
import django.utils.timezone as tz

# Create your models here.


class Event(models.Model):
    nBookings = IntegerField(default=0)
    time = TimeField()
    date = DateField()

    def get_bookings(self):
        return booking.Booking.objects.filter(event=self)

    def get_n_taken_bookings(self):
        return self.get_bookings().count()

    def get_n_available_bookings(self) -> int:
        return self.nBookings - self.get_bookings().count()

    def is_available(self) -> bool:
        return self.nBookings > self.get_n_taken_bookings()

from booking.views import EventAddBookingView
from django.urls.conf import re_path
from event.views import EventDetailView, EventOnDateListView
from django.urls import path

urlpatterns = [
    re_path(r'^(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/(?P<day>[0-9]{2})/$',
            EventOnDateListView.as_view(), name='get_events_on_date'),
    re_path(r'^(?:(?P<id>\d+)/)?$',
            EventDetailView.as_view(), name='event_detail'),
    re_path(r'^(?:(?P<id>\d+)/add_booking)?$',
            EventAddBookingView.as_view(), name='event_add_booking'),
]

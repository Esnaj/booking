import { useEffect, useState } from "react";
import { getEventsBookings } from "../API/EventAPI";
import { BookForm } from "./BookForm";
import { Booking, bookingType } from "./Booking";

export type eventType = {
    id: number;
    date: string;
    time: string;
    is_available: boolean;
    nBookings: number;
    n_taken_bookings: number;
};

export function Event(props: {
    event: eventType;
    activeId: number | null;
    setActiveId: (x: number | null) => void;
    updateEvents: () => void;
    isAdmin?: boolean;
}) {
    const isActive = () => props.activeId === props.event.id;
    const renderExtension = () => {
        if (isActive()) {
            if (props.isAdmin ?? false) {
                return <AdminExtension />;
            }
            if (props.event.is_available) {
                return <Extension />;
            }
        }
    };
    const clickHandler = () => {
        if (!props.isAdmin && !props.event.is_available) return;
        if (isActive()) {
            return props.setActiveId(null);
        }
        return props.setActiveId(props.event.id);
    };

    const availability_classes = props.event.is_available
        ? "hover:bg-green-200"
        : "hover:bg-red-400 bg-gray-200";
    const active_classes = isActive()
        ? "bg-blue-300 hover:bg-blue-300 shadow-lg transform scale-105 my-3"
        : "";
    const classes = [
        "border border-gray-200 p-3 px-8 w-full shadow-sm rounded-sm flex flex-row space-x-3 text-gray-700",
        availability_classes,
        active_classes,
    ].join(" ");
    return (
        <div className="flex flex-col">
            <button onClick={clickHandler} className={classes}>
                <span className="mr-auto">{props.event.time}</span>
                <span className="">
                    {props.event.n_taken_bookings}/{props.event.nBookings}
                </span>
            </button>
            {renderExtension()}
        </div>
    );

    function Extension() {
        return (
            <div className="flex flex-col w-full border shadow-sm border-gray p-3 my-3 text-gray-700">
                <BookForm id={props.event.id} updateEvents={props.updateEvents} />
            </div>
        );
    }

    function AdminExtension() {
        const [bookings, setBookings] = useState<bookingType[]>([]);
        useEffect(() => {
            getEventsBookings(props.event.id).then((e) => {
                setBookings(e);
            });
        }, []);

        return (
            <div className="flex flex-col w-full border shadow-sm border-gray p-3 my-3 text-gray-700">
                {bookings.map((e: bookingType, i) => {
                    return <Booking key={i} booking={e} />;
                })}
            </div>
        );
    }
}

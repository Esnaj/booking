import { useState } from "react";
import Calendar from "react-calendar";

export default function MyCalendar(props: { date: Date; setDate: (date: Date) => void }) {
    const [date, setDate] = useState<string>();
    const today = new Date();
    const maxDate = new Date();
    maxDate.setDate(today.getDate() + 21);
    const test = (date: Date) => {
        const options = { day: "2-digit", month: "2-digit", year: "numeric" } as const;
        setDate(date.toLocaleDateString("nl", options).replaceAll("-", "/"));
    };

    return (
        <>
            <Calendar
                value={props.date}
                onChange={(e: Date) => {
                    props.setDate(e);
                    // console.log(e);
                    test(e);
                }}
                minDate={today}
                maxDate={maxDate}
            />
            <div>
                <span>{date}</span>
            </div>
        </>
    );
}

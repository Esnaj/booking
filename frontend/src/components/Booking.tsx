export type bookingType = { id: number; name: string; email: string };

export function Booking(props: { booking: bookingType }) {
    return (
        <div className="flex flex-col">
            <div className="grid grid-cols-2 space-x-3 border p-3">
                <span>Name: {props.booking.name}</span>
                <span>Email: {props.booking.email}</span>
            </div>
        </div>
    );
}

import { Field, Form, Formik } from "formik";
import { postBookingToEvent } from "../API/EventAPI";
import { bookingSchema } from "./Schemas";

export function BookForm(props: { id: number; updateEvents: () => void }) {
    return (
        <Formik
            initialValues={{
                name: "",
                email: "",
            }}
            onSubmit={(values, { resetForm, setSubmitting }) => {
                console.log("Submitting...");
                postBookingToEvent(props.id, values).then(() => {
                    resetForm();
                    props.updateEvents();
                    alert("Booking submitted succesfuly!");
                    setSubmitting(false);
                });
            }}
            validationSchema={bookingSchema}>
            {({ isSubmitting, errors }) => {
                return (
                    <Form className="flex flex-col bg-gray-50 bg-opacity-50 p-2 space-y-2 rounded-sm">
                        <div className="flex flex-wrap flex-col space-y-2">
                            <MyField name="name" />
                            <MyField name="email" />
                        </div>
                        <button
                            type="submit"
                            disabled={isSubmitting}
                            className="border border-gray-2 p-3 w-32 bg-blue-400 self-end">
                            Toevoegen
                        </button>
                    </Form>
                );
            }}
        </Formik>
    );
    function MyField(props: { name: string }) {
        return (
            <div className="grid grid-cols-2 w-1/2">
                <label className="justify-self-end mr-3 self-center">{props.name}: </label>
                <Field
                    type="input"
                    name={props.name}
                    className="border border-gray-300 rounded-sm shadow-sm px-1 py-1"
                />
            </div>
        );
    }
}

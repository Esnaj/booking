export type bookingType = {
    name: string;
    id: number;
    nSlots: number;
    slots: slotType[];
    available_slots: number;
};
export type dateBookingType = {
    name: string;
    date: Date;
    time: string;
    id: number;
    nSlots: number;
    slots: slotType[];
    available_slots: number;
};

export type slotType = { name: string; id: number; booking_id: number };

import { Switch, Route } from "react-router-dom";
import CalendarPage from "./Pages/CalendarPage";
import { DateBookingPage } from "./Pages/DateBookingPage";
import Home from "./Pages/BookingPage";
import AdminPage from "./Pages/AdminPage";

function App() {
    return (
        <div className="min-h-screen flex flex-col items-center">
            {/* <Navbar className=""/> */}
            <div className="mt-10 w-2/3">
                <Switch>
                    <Route exact path="/">
                        <Home />
                    </Route>
                    <Route exact path="/Admin">
                        <AdminPage />
                    </Route>
                    <Route exact path="/datebooking">
                        <DateBookingPage />
                    </Route>
                </Switch>
            </div>
        </div>
    );
}

export default App;

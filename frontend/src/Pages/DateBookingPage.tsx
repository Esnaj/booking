import React from "react";
import { useState } from "react";
import { dateBookingType, slotType } from "../misc/types";
import Calendar from "react-calendar";
import { postSlot } from "../API/SlotAPI";
import { postDateBooking, getDateBookingOnDate } from "../API/DateBookingAPI";

interface IState {
    date: Date;
    bookingData: dateBookingType[];
    shouldUpdate: boolean;
}

export class DateBookingPage extends React.Component<{}, IState> {
    today: Date;
    maxDate: Date;

    constructor(props: {}) {
        super(props);

        this.today = new Date();
        this.maxDate = new Date();
        this.maxDate.setDate(this.today.getDate() + 100);
        this.state = {
            date: this.today,
            bookingData: [],
            shouldUpdate: false,
        };
    }
    async update() {
        getDateBookingOnDate(this.state.date).then((e: dateBookingType[]) => {
            console.log(e);
            this.setState({
                bookingData: e ?? [],
            });
        });
    }
    shouldUpdate() {
        this.setState({ shouldUpdate: true });
    }

    componentDidMount() {
        getDateBookingOnDate(this.state.date)
            .then((bookingData: any) => {
                console.log(bookingData);
                this.setState({
                    bookingData: bookingData ?? [],
                });
            })
            .catch((err: any) => {
                console.log(err);
            });
    }

    componentDidUpdate(prevProps: {}, prevState: IState) {
        if (this.state.shouldUpdate) {
            // console.log(this.props)
            this.update();
            this.setState({
                shouldUpdate: false,
            });
        }
    }

    render() {
        return (
            <div>
                <Calendar
                    value={this.state.date}
                    onChange={(e) => {
                        this.setState({
                            date: e,
                            shouldUpdate: true,
                        });
                        console.log(e);
                    }}
                    minDate={this.today}
                    maxDate={this.maxDate}
                />
                <div className="flex flex-col items-center justify-center h-screen w-full ">
                    <div className="flex flex-col w-1/2 bg-blue-300">
                        {this.state.bookingData.map((e, i) => {
                            return (
                                <BookingData
                                    key={i}
                                    data={e}
                                    update={this.shouldUpdate.bind(this)}
                                />
                            );
                            // return <div key={e["id"]}>booking name: {e["name"]}; with id: {e['id']}</div>;
                        })}
                    </div>
                    <button
                        className="bg-red-400"
                        onClick={async () => {
                            await postDateBooking(this.state.date);
                            this.update();
                        }}>
                        add booking
                    </button>{" "}
                </div>
            </div>
        );
    }
}
function BookingData(props: { data: dateBookingType; update: () => void }) {
    // console.log(props.data);
    let [isExtended, setIsExtended] = useState(false);
    return (
        <div className="bg-red-200 px-3">
            <div className="flex flex-row w-full ">
                <span>{props.data["name"]} </span>
                <span>{props.data.date}</span>

                <span>----{props.data.time}</span>
                <div className="ml-auto">
                    <span className="">
                        {props.data["slots"].length}/{props.data["nSlots"]}
                    </span>
                    <button
                        className="bg-gray-200 rounded-md shadow-md px-1 ml-2"
                        onClick={() => setIsExtended(!isExtended)}>
                        {isExtended ? "^" : "v"}
                    </button>
                </div>
            </div>
            <BookingDataExtension />
        </div>
    );

    function BookingDataExtension() {
        if (isExtended) {
            return (
                <div className="bg-gray-100 w-3/4">
                    {props.data.slots.map((e, i) => {
                        return <Slot key={i} {...e} />;
                    })}
                    <div>
                        <span>Do you wish to book this slot?</span>
                        <button
                            className="bg-blue-300 rounded-md shadow-md ml-auto"
                            onClick={async () => {
                                await postSlot(props.data.id);
                                props.update();
                            }}>
                            YES!!!
                        </button>
                        <button
                            onClick={() => {
                                console.log(props.data.id);
                            }}>
                            test
                        </button>
                    </div>
                </div>
            );
        }
        return <></>;
    }
    function Slot(props: slotType) {
        return (
            <div className="flex flex-row space-x-10">
                <span>slot_Id: {props.id}</span>
                <span>name: {props.name}</span>
            </div>
        );
    }
}

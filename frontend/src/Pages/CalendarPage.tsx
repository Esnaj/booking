import { useState } from "react";
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";

export default function CalendarPage() {
    const [date, setDate] = useState(new Date());
    const today = new Date();
    const maxDate = new Date();
    maxDate.setDate(today.getDate() + 21);

    return (
        <div>
            <Calendar
                value={date}
                onChange={(e) => {
                    setDate(e);
                    console.log(e);
                }}
                minDate={today}
                maxDate={maxDate}></Calendar>
            <button
                onClick={() => {
                    console.log(maxDate);
                }}>
                {date.getFullYear()}
            </button>
        </div>
    );
}

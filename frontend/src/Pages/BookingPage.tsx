import { useEffect, useState } from "react";
import { getEventsOnDate } from "../API/EventAPI";
import { Event, eventType } from "../components/Event";
import MyCalendar from "../components/MyCalendar";

export default function Home(): any {
    const [activeId, setActiveId] = useState<number | null>(null);
    const [eventData, setEventData] = useState<eventType[]>([]);
    const [date, setDate] = useState<Date>(new Date());

    useEffect(updateEvents, [date]);

    function updateEvents() {
        setActiveId(null)
        getEventsOnDate(date).then((x: any) => {
            setEventData(x.data ?? []);
        });
    }

    return (
        <div className=" w-full flex flex-col items-center">
            <MyCalendar date={date} setDate={setDate} />
            <div className="flex flex-col w-1/2 justify-center space-y-1 mt-5 mb-10">
                {eventData.map((e, i) => {
                    return (
                        <Event
                            key={e.id}
                            event={e}
                            activeId={activeId}
                            setActiveId={setActiveId}
                            updateEvents={updateEvents}
                        />
                    );
                })}
            </div>
        </div>
    );
}

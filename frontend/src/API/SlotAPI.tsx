import axios from "axios";

export async function postSlot(booking_id: number) {
    return axios
        .post("http://127.0.0.1:8000/booking/booking_id-" + booking_id + "/slot", {
            name: "booking_slot",
        })
        .then((res: any) => {})
        .catch((err: any) => {
            alert("Adding a slot failed!");
            console.log(err);
        });
}

import axios from "axios";

export async function getEventsOnDate(date: Date) {
    const options = { year: "numeric", month: "2-digit", day: "2-digit" } as const;
    const dateArray = date.toLocaleDateString("nl", options).split("-");
    const formatted_date = [dateArray[2], dateArray[1], dateArray[0]].join("/");
    console.log(formatted_date);
    const url = "http://127.0.0.1:8000/event/" + formatted_date + "/";
    return axios
        .get(url)
        .then((res: any) => {
            return res;
        })
        .catch((err: any) => {
            console.log(err);
            return [];
        });
}

export async function postBookingToEvent(id: number, data: { name: string; email: string }) {
    const url = "http://127.0.0.1:8000/event/" + id + "/add_booking";
    return axios
        .post(url, data)
        .then((res: any) => {
            return res;
        })
        .catch((err: any) => {
            console.log(err);
        });
}

export async function getEventsBookings(id: number) {
    const url = "http://127.0.0.1:8000/event/" + id + "/";
    return axios
        .get(url)
        .then((res: any) => {
            return res.data["bookings"];
        })
        .catch((err: any) => {
            console.log(err);
            return [];
        });
}

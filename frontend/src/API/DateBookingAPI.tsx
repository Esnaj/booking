import axios from "axios";

export async function getDateBookingList() {
    return axios
        .get("http://127.0.0.1:8000/datetimebooking/")
        .then((res: any) => {
            return res.data;
        })
        .catch((error: any) => {
            console.log(error);
        });
}

export async function postDateBookingToday() {
    let today = new Date().toISOString().slice(0, 10);
    return axios
        .post("http://127.0.0.1:8000/datetimebooking/", { name: "datebooking", date: today })
        .then((res: any) => {
            return res;
        })
        .catch((err: any) => {
            console.log(err);
        });
}

export async function postDateBooking(date: Date) {
    const formatted_date = date.toISOString().slice(0,10)
    return axios
        .post("http://127.0.0.1:8000/datetimebooking/", { name: "datebooking", date: formatted_date  })
        .then((res: any) => {
            return res;
        })
        .catch((err: any) => {
            console.log(err);
        });
}

export async function getDateBookingOnDate(date: Date) {
    let formatted_date = formatDate(date);
    return axios
        .get("http://127.0.0.1:8000/datetimebooking/" + formatted_date + "/")
        .then((res: any) => {
            return res.data;
        })
        .catch((err: any) => {
            console.log(err);
        });
}

function formatDate(date: Date) {
    var d = new Date(date),
        month = "" + (d.getMonth() + 1),
        day = "" + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [year, month, day].join("/");
}

import axios from "axios";

export async function getBookingList() {
    return axios
        .get("http://127.0.0.1:8000/booking/")
        .then((res: any) => {
            return res.data;
        })
        .catch((error: any) => {
            console.log(error);
        });
}

export async function postBooking() {
    return axios
        .post("http://127.0.0.1:8000/booking/", { name: "booking" })
        .then((res: any) => {
            return res;
        })
        .catch((err: any) => {
            console.log(err);
        });
}

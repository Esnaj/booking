import {Link} from "react-router-dom";

export default function Navbar() {
    return (
        <nav>
            <ul className="flex flex-row space-x-10">
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/datebooking">Datebooking</Link>
                </li>
                <li>
                    <Link to="/calendar">Calendar</Link>
                </li>
            </ul>
        </nav>
    );
}
